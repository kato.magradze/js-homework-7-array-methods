//#1
let names = ["George", "Nick", "Tom", "Kate", "Annie"];

let friends = arr => arr.filter(name => name.length === 4);
console.log(friends(names));

//without a function:
// let friends = names.filter(name => name.length === 4);
// console.log(friends);

//#2
let nums = [5, 51, 12, 19, 22];

function sumOfSmallest(arr) {
    arr.sort((a,b) => a-b);
    arr = arr.slice(0, 2);
    return arr.reduce((sum, currentVal) => sum + currentVal);
}
console.log(sumOfSmallest(nums));